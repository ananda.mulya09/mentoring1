<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function daftar_mahasiswa( )
    {
        return view('index', [
            'title' => 'Daftar Mahasiswa',
            'mahasiswa'=> Mahasiswa::all(),
        ]);
    }

    public function tambah_mahasiswa(Request $request)
    {
        $mahasiswa=new Mahasiswa;
        $mahasiswa->nama=$request->nama;
        $mahasiswa->nim=$request->nim;
        $mahasiswa->jenis_kelamin=$request->jenis_kelamin;
        $mahasiswa->prodi=$request->prodi;
        $mahasiswa->fakultas=$request->fakultas;
        $mahasiswa->save();

        return redirect('/daftar_mahasiswa')->with("tambah_mahasiswa","Tambah Data Berhasil!");
    }

    public function delete_mahasiswa($id)
    {
        Mahasiswa::find($id)->delete();
        return redirect()->back()->with("delete_anggota","Data Mahasiswa Berhasil di Hapus!");
    }

    public function update_mahasiswa($id)
    {
        return view('update_mahasiswa', [
            'title' => 'Update Mahasiswa',
            'mahasiswa'=> Mahasiswa::find($id),
        ]);
    }

    public function edit_mahasiswa($id, Request $request)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->update($request->only(['nama','nim','jenis_kelamin','prodi','fakultas']));
        if ($mahasiswa->save()){
            return redirect('/daftar_mahasiswa')->with('edit_mahasiswa', 'Data Mahasiswa Berhasil Diupdate');
        }
    }
}

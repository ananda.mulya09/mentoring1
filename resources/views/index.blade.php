<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Data Mahasiswa</title>
</head>
<body>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Mahasiswa</h3>

          <div class="card-tools">
            <div class="col-12">
                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#tambah_anggota">
                    <i class="bi bi-plus-square"></i> + Tambah Data
                </button>
            </div>
          </div>
        </div>
        {{-- modal tambah --}}
        <div class="modal fade" id="tambah_anggota" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <form action="/tambah_mahasiswa" id="" method="post">
                    @csrf
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Data</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Nama</label>
                                <input type="text" min="1" name="nama" id="site_title_inp" class="form-control shadow-none" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">NIM</label>
                                <input type="text" min="1" name="nim" id="site_title_inp" class="form-control shadow-none">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Prodi</label>
                                <input type="text" min="1" name="prodi" id="site_title_inp" class="form-control shadow-none" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Fakultas</label>
                                <input type="text" min="1" name="fakultas" id="site_title_inp" class="form-control shadow-none" required>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Laki - Laki" required>
                                    <label class="form-check-label" for="jenis_kelamin">
                                    Laki - Laki
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" required>
                                    <label class="form-check-label" for="jenis_kelamin" aria-describedby="">
                                    Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn text-secondary shadow-none" data-bs-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-success text-white shadow-none">Kirim</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body p-3">
            <table class="table table-striped projects" id="dataTable">
                <thead>
                    <tr>
                        <th>
                            Nama
                        </th>
                        <th>
                            NIM
                        </th>
                        <th>
                            Jenis Kelamin
                        </th>
                        <th>
                            Prodi
                        </th>
                        <th>
                            Fakultas
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mahasiswa as $item)
                    <tr>
                        <td>
                            {{ $item->nama }}
                        </td>
                        <td>
                            {{ $item->nim }}
                        </td>
                        <td>
                            {{ $item->jenis_kelamin }}
                        </td>
                        <td>
                            {{ $item->prodi }}
                        </td>
                        <td>
                            {{ $item->fakultas }}
                        </td>
                        <td class="project-actions text-right">
                            <a class="btn btn-info btn-sm" href="/mahasiswa/{{ $item->id }}/edit">
                                <i class="fas fa-pencil-alt"></i> Edit
                            </a>
                            <a class="btn btn-danger btn-sm" href="/mahasiswa/{{ $item->id }}/delete" onclick="return confirm('Apakah yakin ingin menghapus?')">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
      </div>
</body>
</html>

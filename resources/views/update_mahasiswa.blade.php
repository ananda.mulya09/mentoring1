<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Data Mahasiswa</title>
</head>
<body>
    <div class="card">
        <div class="card shadow mb-3">
            <div class="card-header py-3">
                <p class="text-primary m-0 fw-bold">Data Mahasiswa</p>
            </div>
            <div class="card-body">
                <form action="update_mahasiswa" id="rooms-setting" method="post" enctype="multipart/form-data">
                    @csrf
                    {{-- <input type="hidden" name="id" value="{{$pengurus->id}}"> --}}
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="mb-3">
                                <label class="form-label" for="nama_anggota"><strong>Nama</strong></label>
                                <input class="form-control" type="text" id="nama" value="{{$mahasiswa->nama}}" name="nama">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="mb-3">
                                <label class="form-label" for="no"><strong>NIM</strong></label>
                                <input class="form-control" type="text" id="nim" value="{{$mahasiswa->nim}}" name="nim">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Laki - Laki" required>
                                <label class="form-check-label" for="jenis_kelamin">
                                Laki - Laki
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" required>
                                <label class="form-check-label" for="jenis_kelamin" aria-describedby="">
                                Perempuan
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="mb-3">
                                <label class="form-label" for="pukul"><strong>Prodi</strong></label>
                                <input class="form-control" type="text" id="prodi" value="{{$mahasiswa->prodi}}" name="prodi">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="mb-3">
                                <label class="form-label" for="keterangan"><strong>Fakultas</strong></label>
                                <input class="form-control" type="text" id="fakultas" value="{{$mahasiswa->fakultas}}" name="fakultas">
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="modal-footer">
                            <button id="submitButton" name="kirim" class="btn btn-outline-dark w-175 shadow-none">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

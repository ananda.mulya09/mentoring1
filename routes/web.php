<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/daftar_mahasiswa', [MahasiswaController::class,'daftar_mahasiswa'])->name('daftar_mahasiswa');
Route::post('/tambah_mahasiswa', [MahasiswaController::class,'tambah_mahasiswa'])->name('tambah_mahasiswa');
Route::get('/mahasiswa/{id}/edit', [MahasiswaController::class, 'update_mahasiswa'])->name('update_mahasiswa');
Route::post('/mahasiswa/{id}/update_mahasiswa', [MahasiswaController::class, 'edit_mahasiswa'])->name('edit_mahasiswa');
Route::get('/mahasiswa/{id}/delete', [MahasiswaController::class, 'delete_mahasiswa'])->name('delete_mahasiswa');
